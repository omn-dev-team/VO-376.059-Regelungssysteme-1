\begin{question}[section=1,subtitle={Least Squares (LSQ)},difficulty=4,mode=exm,type=mdl,tags={}]
\begin{compactenum}
	\item Leiten Sie den Parametervektor $ \V{p}$ her.
	\item Wieso darf man $a_0=1$ wählen?
	\item Was ist die Idee beim Least Squares Verfahren?
	\item Welche Vorraussetzungen müssen gegeben sein?
	\item Was passiert, wenn $\M{S}^T \M{S}$ singulär ist?
	\item Wie sieht die Lösung aus?
	%\item Wieso wird die euklidische Norm $||.||_2$ benötigt? %Verwirrend...
	\end{compactenum}
\end{question}
\begin{solution}
\newline\noindent\textbf{Herleitung vom Parametervektor}\newline
Ausgehend von einem ARMAX-Modell der Form 
\begin{equation}
	y_k = \frac{B(\delta)}{A(\delta)} u_k + \frac{C(\delta)}{A(\delta)} w_k
\end{equation}
bekommt man nach Anwendung der Operatoren
\begin{align}
	a_0 y_k +a_1 y_{k-1} + &\cdots + a_n y_{k-n} = \\ &b_0 u_k +b_1 u_{k-1} + \cdots + b_m u_{k-m} + c_0 w_k +c_1 w_{k-1} + \cdots + c_l w_{k-l}
\end{align}
bzw.
\begin{align}
	a_0 y_k = -a_1 y_{k-1} - &\cdots - a_n y_{k-n} + b_0 u_k +b_1 u_{k-1} + \cdots + b_m u_{k-m} \\ 
	& + c_0 w_k +c_1 w_{k-1} + \cdots + c_l w_{k-l}
\end{align}
und mit $a_0$ durchdividiert
\begin{align}
	y_k = \frac{1}{a_0} (-a_1 y_{k-1} - & \cdots - a_n y_{k-n} + b_0 u_k +b_1 u_{k-1} + \cdots + b_m u_{k-m} \\
	& + c_0 w_k +c_1 w_{k-1} + \cdots + c_l w_{k-l} ).
\end{align}
Man erkennt dass der herausgehobene Parameter $a_0$ ein redundanter Parameter ist, und setzt $a_0 = 1$.
\noindent
Diese Gleichung soll in eine kompaktere Schreibweise
\begin{equation}
	y_k = \V{s}_k^T \V{p}.
\end{equation}
gebracht werden. Dafür setzt man 
\begin{align}
	\V{s}_k = \left[ \begin{array}{c}  y_{k-1} \\ \vdots \\ y_{k-n} \\u_k \\ u_{k-1} \\ \vdots \\ u_{k-l} \\ w_k \\ w_{k-1} \\ \vdots \\ w_{k-m} 
	\end{array} \right] \hspace{1cm} \text{und} \hspace{1cm}
	\V{p} = \left[ \begin{array}{c}  -a_1 \\ \vdots \\ -a_n \\b_0 \\ b_1 \\ \vdots \\ b_l \\ c_0 \\ c_1 \\ \vdots \\ c_m 
	\end{array} \right]
\end{align}
\newline\noindent\textbf{Messungen}\newline
Man macht jetzt weitere Messungen, insgesamt $m$ Stück. Die Matrix $\M{S}$ besteht dann zeilenweise aus den $m$ Transponierten Messvektoren $\V{s}_k^T$.
Da man im Allgemeinem mehr Messungen macht als man zu bestimmende Parameter hat ($m>n$), ergibt sich ein überstimmtes Gleichungsystem 
\begin{equation}
	\V{y} = \M{S}\V{p}
\end{equation}
der Form
\begin{equation}
	\begin{bmatrix}
		y_1 \\ y_2 \\ \vdots \\ y_m
	\end{bmatrix}^{m \times 1} = 
	\begin{bmatrix}
		\V{s}^T_1 \\ \V{s}^T_2 \\ \vdots \\ \V{s}^T_m
	\end{bmatrix}^{m \times n}
	\begin{bmatrix}
		p_1 \\ \vdots \\ p_n
	\end{bmatrix}^{n \times 1}
\end{equation}
Anm.: In die Lösbarkeit des Gleichungssystems geht natürlich der Rang der Matrizen mit ein. Um eine eindeutige Lösung zu bekommen, müsste man $\V{y}$ als Linearkombination der Spalten von $\M{S}$ darstellen können, d.h. $\mathrm{rang}(\M{S}) = \mathrm{rang}([\M{S},\V{y}]) $ müsste gelten.

\noindent\textbf{Grundidee}\newline
Da es keine eindeutige Lösung gibt, definieren wir einen quadratischen Fehler, und suchen diesen zu minimieren:
\begin{equation}
	\min_{\V{p}} ||\V{e}||_2^2 \hspace{1cm} \text{mit} \hspace{1cm} \V{e} = \V{y} - \M{S}\V{p}
\end{equation}
Das Minimum einer quadratischen Gütefunktion lässt sich berechnen, indem man die Ableitung Null setzt (quad. Funktionen haben ein eindeutiges globales Minimum).
\begin{align}
	\PartDiff{}{\V{p}}\V{e}^T\V{e} &= \PartDiff{}{\V{p}}(\V{y} - \M{S}\V{p})^T(\V{y} - \M{S}\V{p}) = \V{0}^T\\
	& = \PartDiff{}{\V{p}} \left( \V{y}^T\V{y} - \V{y}^T\M{S}\V{p} - \V{p}^T\M{S}^T\V{y} + \V{p}^T\M{S}^T\M{S}\V{p}\right)\\
	& = -2 \V{y}^T\M{S} + 2 \M{S}^T\M{S} \V{p} = \V{0}^T,
\end{align}
wobei die Symmetrie von $\M{S}^T\M{S}$ ausgenutzt wurde.
Daraus folgt die optimale Lösung $\V{p}_0$ zu
\begin{align}
	\V{p}_0 = & (\M{S}^T\M{S})^{-1}\V{y}^T\M{S}\\
	= & \underbrace{(\M{S}^T\M{S})^{-1}\M{S}^T}_{\M{S}^{\dag}} \V{y}
\end{align}
mit der Pseudoinversen $\M{S}^{\dag}$ von $\M{S}$.

\noindent\textbf{Zur Pseudoinversen}\newline
Es muss die Inverse der Matrix $\M{S}^T\M{S}$ berechnet werden. Diese Matrix hat immer die Dimension $n \times n$. Sie ist invertierbar, wenn $\M{S}$ spaltenregulär ist, d.h. die Parameter müssen voneinender linear unabhängig sein. (Matrix regulär $\to$ Inverse exisitert, Matrix nur spaltenregulär $\to$ Pseudoinverse existiert)
\end{solution}
